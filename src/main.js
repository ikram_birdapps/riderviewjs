import Vue from "vue";
import App from "./App.vue";
import { BootstrapVue, IconsPlugin } from "bootstrap-vue";
import VueRouter from "vue-router";
import VueToastr from "vue-toastr";
import * as VueGoogleMaps from "vue2-google-maps";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";
import helpers from "./utils/helpers";
import Vuetify from "vuetify";
import "./utils/filters";

export const eventBus = new Vue();
const plugin = {
  install() {
    Vue.helpers = helpers;
    Vue.prototype.$helpers = helpers;
  }
};

Vue.use(plugin);

import { routes } from "./routes";
import "./registerServiceWorker";

Vue.use(Vuetify);
Vue.use(VueRouter);
Vue.use(BootstrapVue);
Vue.use(IconsPlugin);
Vue.use(VueToastr, {
  /* OverWrite Plugin Options if you need */
  defaultClassNames: ["animated", "zoomInUp"]
  //defaultTimeout: 300000
});

Vue.use(VueGoogleMaps, {
  load: {
    key: "AIzaSyAbVEJf5_oaasC4qW_Cu0s9cMbRcHrdkjc", //"AIzaSyCMbdNGxkjboY7rmXoamglb1ivT7demhIg", //"AIzaSyAbVEJf5_oaasC4qW_Cu0s9cMbRcHrdkjc", //"AIzaSyCQ275l2nxna9WhbXnzCxsu6HQLKOWC2tY", //AIzaSyDR-baLQfHEcBAyL858Ufds3ysOptyPTCg
    libraries: "places" // necessary for places input
  }
});

export const router = new VueRouter({
  //base: "/touchless_admin/",
  // base: "/",
  routes,
  mode: "hash" //"history"
});

// export const router;

Vue.config.productionTip = false;

new Vue({
  router,
  render: h => h(App)
}).$mount("#app");
