import http from "../http-common";

class Order {
  profileupdate(payLoad) {
    return http.post("profile/profileupdate", payLoad);
  }

  getUserProfile(payLoad) {
    return http.post("profile/profile", payLoad);
  }
  profileUpload(payLoad) {
    return http.post("profile/updateprofilepicture", payLoad);
  }
  manageAddress(userId) {
    return http.get(`location/customerAddress?userId=${userId}`);
  }
  profileDelete(payLoad) {
    return http.post("profile/deleteAddress", payLoad);
  }
  contactUs(payLoad) {
    return http.post("webpage/contact_us_email", payLoad);
  }
}

export default new Order();
