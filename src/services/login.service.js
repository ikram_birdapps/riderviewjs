import http from "../http-common";

class Login {
  requestOTP(payLoad) {
    return http.post("rider/login", payLoad);
  }

  verifyOTP(payLoad) {
    return http.post("rider/verifyOTP", payLoad);
  }
}

export default new Login();
