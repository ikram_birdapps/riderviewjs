import http from "../http-common";

class HomeRequests {
  requestHome(userId) {
    return http.get(`home/home?userId=${userId}`);
  }
  getCall(payLoad) {
    return http.post("OrdersCall/ordersCallSave", payLoad);
  }
  serviceBenifits() {
    return http.get(`home/service_benefits`);
  }
  getAddress(userId) {
    return http.get(`location/customerAddress?userId=${userId}`);
  }
  orderRating(payLoad) {
    return http.post("order/orderRating", payLoad);
  }
}

export default new HomeRequests();
