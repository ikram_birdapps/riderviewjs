import http from "../http-common";

class Location {
  saveLocation(payLoad) {
    return http.post("location/saveLocation", payLoad);
  }
}

export default new Location();
