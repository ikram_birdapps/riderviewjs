import http from "../http-common";

class Order {
  timeSlot(payLoad) {
    return http.post("slot/slot", payLoad);
  }

  placeOrder(payLoad) {
    return http.post("order/order", payLoad);
  }
  getOrderStatus(payLoad) {
    return http.post("order/orderitemdetails", payLoad);
  }
  getDeliveryDuties(payLoad) {
    return http.post("rider/duties", payLoad);
  }
  getPickupDuties(payLoad) {
    return http.post("rider/duties", payLoad);
  }
  getItemsAutocomplete(payLoad) {
    return http.post("order/itemSearch", payLoad);
  }
  getOrderDetails(payLoad) {
    return http.post("order/details", payLoad);
  }
  getOrderCalculation(payLoad) {
    return http.post("order/getOrderCalculation", payLoad);
  }
  orderSave(payLoad) {
    return http.post("order/orderItemSave", payLoad);
  }
  changeOrderStatus(payLoad) {
    return http.post("order/rider_update_order_status", payLoad);
  }
}

export default new Order();
