import http from "../http-common";

class HomeRequests {

  getServiceList(payLoad) {
    return http.post("serviceItems/serviceList", payLoad);
  }
  getServiceCategories(payLoad) {
    return http.post("serviceItems/itemList", payLoad);
  }
}

export default new HomeRequests();
