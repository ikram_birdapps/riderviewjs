import http from "../http-common";

class SignatureService {
  signatureUpload(payLoad) {
    return http.post("user/uploadSignatureMedia", payLoad);
  }
}

export default new SignatureService();
