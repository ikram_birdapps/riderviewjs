export default {
  /**
   * Using helpers
   * this.$helpers.getUserId()
   * https://stackoverflow.com/questions/42613061/vue-js-making-helper-functions-globally-available-to-single-file-components
   */
  capFirstLetter(val) {
    return val.charAt(0).toUpperCase() + val.slice(1);
  },
  getPickupAddress() {
    let address = JSON.parse(localStorage.getItem("PICKUP_ADDRESS"));
    //console.log("PICKUP_ADDRESS" + address);

    return address;
  },
  getDeliveryAddress() {
    let address = JSON.parse(localStorage.getItem("DELIVERY_ADDRESS"));
    // console.log("DELIVERY_ADDRESS" + address);
    return address;
  },
  saveLocationDetails(data) {
    let _data = JSON.stringify(data);
    localStorage.setItem("LOCATION_DETAILS", _data);
  },
  getPostcode() {
    let details = JSON.parse(localStorage.getItem("LOCATION_DETAILS"));
    if (details && details.postcode) return details.postcode;
    return "";
  },
  saveLaundryIDs(data) {
    let _data = JSON.stringify(data);
    localStorage.setItem("LAUNDRY_IDS", _data);
  },
  getLaundryIds() {
    let _data = JSON.parse(localStorage.getItem("LAUNDRY_IDS"));
    if (_data) return _data[0];
    return "";
  },


  //////Rider details ///////////

  saveRiderDetails(_data) {
    //let _data = JSON.parse(data);
    localStorage.setItem("RIDER_ID", _data.rider_id);
    localStorage.setItem("RIDER_MOBILE", _data.mobile);
    localStorage.setItem("RIDER_EMAIL", _data.email);
    localStorage.setItem("RIDER_NAME", _data.name);
    localStorage.setItem("RIDER_IMAGE", _data.profile_image);
    localStorage.setItem("SUPPORT", _data.support);

  },

  getRiderId() {
    return localStorage.getItem("RIDER_ID");
  },
  getRiderName() {
    return localStorage.getItem("RIDER_NAME");
  },
  getRiderMobile() {
    return localStorage.getItem("RIDER_MOBILE");
  },
  getRiderEmail() {
    return localStorage.getItem("RIDER_EMAIL");
  },
  getSupportContact() {
    let _data = JSON.parse(localStorage.getItem("SUPPORT"));
    if (_data) return _data.get("contactus");
    return "";
  },

  getSupportWhatsApp() {
    let _data = JSON.parse(localStorage.getItem("SUPPORT"));
    if (_data) return _data.get("whatsapp");
    return "";
  },

  getSupportEmail() {
    let _data = JSON.parse(localStorage.getItem("SUPPORT"));
    if (_data) return _data.get("email");
    return "";
  },

  getBecomePartnerLink() {
    let _data = JSON.parse(localStorage.getItem("SUPPORT"));
    if (_data) return _data.get("become_a_partner");
    return "";
  },
  deleteLocalStorageAfterOrder() {
    let items = [
      "PICKUP_TIME",
      "PICKUP_ADDRESS",
      "DELIVERY_ADDRESS",
      "LOCATION_DETAILS",
      "LAUNDRY_IDS",
      "PICKUP_DATE",
      "DELIVERY_TIME",
      "DELIVERY_DATE"
    ];
    for (let i = 0; i < items.length; i++) {
      localStorage.removeItem(items[i]);
    }
  },
  getOrderStatus(statusId) {
    switch (statusId) {
      case 1:
        return "Confirmed";
      case 2:
        return "";
      case 3:
        return "Picked Up";
      case 4:
        return "Processing";
      case 5:
        return "Ready to Dispatch";
      case 6:
        return "Out For Delivery";
      case 7:
        return "Delivered";
      case 8:
        return "Cannot Pickup";
      case 9:
        return "Could not Pickup - Wrong Address";
      case 10:
        return "Order Modified";
      case 11:
        return "Cancelled";
      case 12:
        return "New Order";
      case 13:
        return "Could not Pickup - unforeseen circumstances";
      case 14:
        return "Could not Pickup - non-availability";
      case 15:
        return "Request Qc";
      case 19:
        return "Item Collected";
      case 20:
        return "Ready to be Picked";
    }
  }
};
