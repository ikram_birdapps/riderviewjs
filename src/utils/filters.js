import Vue from "vue";

Vue.filter("dateTwoDigit", function (date) {
  var d = date.slice(-2);
  return d;
});

Vue.filter("weekDayName", function (date) {
  let days = ["Sun", "Mon", "Tues", "Wed", "Thu", "Fri", "Sat"];
  let d = new Date(date);
  let dd = d.getDay();
  return days[dd];
});

Vue.filter("textLength", function (text) {
  let length = text.size
  if (length > 10)
    return text.substring(0, 10);
  return text;
});

Vue.filter("tConvert", function (time) {
  let service_from = time.service_from;
  let service_to = time.service_to;
  let am_pm_from = "";
  let am_pm_to = "";

  //time[0] = +time[0] % 12 || 12; // Adjust hours
  if (service_from < 12) {
    am_pm_from = " AM";
  } else {
    am_pm_from = " PM";
  } // Set AM/PM

  if (service_to < 12) {
    am_pm_to = " AM";
  } else {
    am_pm_to = " PM";
  } // Set AM/PM

  service_from = +service_from % 12 || 12;
  service_to = +service_to % 12 || 12;

  return service_from + am_pm_from + " to " + service_to + am_pm_to;
});

Vue.filter("orderDate", function (date) {
  if (!date) return "";
  let days = ["Sun", "Mon", "Tues", "Wed", "Thu", "Fri", "Sat"];
  let month = {
    "01": "Jan",
    "02": "Feb",
    "03": "Mar",
    "04": "Apr",
    "05": "May",
    "06": "Jun",
    "07": "Jul",
    "08": "Aug",
    "09": "Sep",
    "10": "Oct",
    "11": "Nov",
    "12": "Dec"
  };
  let d = new Date(date);
  let dd = d.getDay(); // get date like Sun, Mon  //
  var dt = date.slice(-2);
  var mon = date.slice(5, 7);
  return `${days[dd]}, ${dt} ${month[mon]}`;
});
