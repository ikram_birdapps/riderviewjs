import Login from "./components/login/Login.vue";
import Otp from "./components/login/Otp.vue";
import Orders from "./components/orders/Orders.vue";
import EditOrder from "./components/orders/EditOrder.vue";
import OrderDetail from "./components/orders/OrderDetail.vue";
import SignaturePad from "./components/signature/SignaturePad.vue";
import OrderAdd from "./components/orders/OrderAdd.vue";
import helpers from "./utils/helpers";
const auth = function (to, from, next) {
  const currentUser = helpers.getRiderId();
  if (!currentUser) {
    next("/login");
  } else {
    next();
  }
};

export const routes = [
  { path: "", component: Login },
  { path: "/login", component: Login },
  { path: "/otp", component: Otp },
  { path: "/orders", component: Orders, beforeEnter: auth },
  { path: "/edit-order/:id", component: EditOrder, beforeEnter: auth },
  { path: "/order-detail/:id", component: OrderDetail, beforeEnter: auth },
  { path: "/signature/:id", component: SignaturePad, beforeEnter: auth },
  { path: "/order-add/:id", component: OrderAdd, beforeEnter: auth }
  //{ path: "/location", component: Location, beforeEnter: auth },
];
