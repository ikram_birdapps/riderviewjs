import axios from "axios";
const instance = axios.create({
  //baseURL: "http://testing.birdapps.org/launderette-v3/api-web-merchant-v1/",

  // baseURL: "http://cms.launderette.staging.birdapps.org/api-web-merchant-v1/",

  baseURL: "https://apilaunderettestaging.birdres.com/api-web-merchant-v1/",
  headers: {
    "Content-type": "application/json"
  },
  auth: {
    username: "admin",
    password: "1234"
  }
});

instance.interceptors.request.use(config => {
  console.log("Interceptors...................................");

  //// router.push({ path: `/login` });

  // const vm = new Vue();
  // vm.$bvToast.toast(`Something went wrong.`, {
  //   title: "Error",
  //   autoHideDelay: 5000,
  //   appendToast: false,
  //   variant: "danger"
  // });

  return config;
});

export default instance;
