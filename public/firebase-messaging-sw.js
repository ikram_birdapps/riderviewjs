importScripts("https://www.gstatic.com/firebasejs/7.8.0/firebase-app.js");
importScripts("https://www.gstatic.com/firebasejs/7.8.0/firebase-messaging.js");
if (firebase.messaging.isSupported()) {
    var payloadData;
    const firebaseConfig = {
        apiKey: "AIzaSyCMbdNGxkjboY7rmXoamglb1ivT7demhIg",
        authDomain: "launderette-36a17.firebaseapp.com",
        databaseURL: "https://launderette-36a17.firebaseio.com",
        projectId: "launderette-36a17",
        storageBucket: "launderette-36a17.appspot.com",
        messagingSenderId: "404726729232",
        appId: "1:404726729232:web:b1c3b8d0769375e5b61895",
        measurementId: "G-XXLXWS4GSB"
    };
    firebase.initializeApp(firebaseConfig);
    const messaging = firebase.messaging();
    messaging.setBackgroundMessageHandler(function (payload) {
        console.log(' Received background message ', payload);
        //alert('Notification received!');
        // var sender = JSON.parse(payload.data.message);
        payloadData = payload;
        var notificationTitle = payload.data.title;
        var notificationOptions = {
            body: payload.data.body,
            icon: payload.data.icon
        };
        return self.registration.showNotification(
            notificationTitle,
            notificationOptions
        );
        // return self.registration.showNotification(payload.notification.title, {
        //     body: payload.notification.body,
        //     data: payload.data.link
        // });
        // return (new Notification(payload.data.title, {
        //     body: payload.data.body,
        //     icon: "https://bit.ly/2DYqRrh"
        // }).onclick = function () {
        //     self.window.focus();
        //     var url = "http://127.0.0.1:8887";
        //     event.waitUntil(
        //         self.clients.matchAll({ type: "window" }).then(windowClients => {
        //             for (var i = 0; i < windowClients.length; i++) {
        //                 var client = windowClients[i];
        //                 if (client.url === url && "focus" in client) {
        //                     console.log("FOCUS SAME WINDOW1" + url);
        //                     alert("FOCUS SAME WINDOW1" + url);
        //                     client.navigate("http://127.0.0.1:8887/#/order-history");
        //                     return client.focus();
        //                 }
        //             }
        //             if (self.clients.openWindow) {
        //                 console.log("FOCUS SAME WINDOW2");
        //                 return self.clients.openWindow(url);
        //             }
        //         })
        //     );
        //     console.log("Background notification clicked");
        //     let route = payload.data.link;
        //     _this.$router.push(route);
        // });
    });
    this.addEventListener('notificationclick', function (event) {
        event.notification.close();
        console.log("Notification=>" + JSON.stringify(event));

        //this.window.focus();
        //var url = "http://127.0.0.1:8887";
        //var url = "http://localhost:8081";
        var url = "https://riderlaunderettestaging.birdres.com/";
        event.waitUntil(
            this.clients.matchAll({ includeUncontrolled: true, type: "window" }).then(windowClients => {
                for (var i = 0; i < windowClients.length; i++) {
                    var client = windowClients[i];
                    if (client.url.includes(url) && "focus" in client) {
                        console.log("FOCUS SAME WINDOW1" + url);
                        //alert("FOCUS SAME WINDOW1" + url);
                        //client.navigate("http://127.0.0.1:8887/#/order-history");
                        //this.$router.push(this.payload.data.link)
                        client.focus();
                        //self.window.open(url + "/#" + this.payload.data.link, "_self");
                        client.postMessage({
                            'payload': this.payloadData,
                            'type': "SERVICE"
                        })
                        return "";
                    }
                }
                if (this.clients.openWindow) {
                    console.log("FOCUS SAME WINDOW2");
                    this.clients.openWindow(url + "/#" + this.payloadData.data.link);
                    openClient(url);
                }
            })
        );
    });
    function openClient(url) {
        this.clients.matchAll({ includeUncontrolled: true, type: "window" }).then(windowClients => {
            for (var i = 0; i < windowClients.length; i++) {
                var client = windowClients[i];
                if (client.url.includes(url) && "focus" in client) {
                    console.log("FOCUS SAME WINDOW1" + url);
                    //alert("FOCUS SAME WINDOW1" + url);
                    //client.navigate("http://127.0.0.1:8887/#/order-history");
                    //this.$router.push(this.payload.data.link)
                    client.focus();
                    //self.window.open(url + "/#" + this.payload.data.link, "_self");
                    client.postMessage({
                        'payload': this.payloadData,
                        'type': "SERVICE"
                    })
                    return "";
                }
            }
            if (this.clients.openWindow) {
                console.log("FOCUS SAME WINDOW2");
                this.clients.openWindow(url + "/#" + this.payloadData.data.link);
                client.postMessage({
                    'payload': payloadData,
                    'type': "notification"
                })
            }
        })
    }
}